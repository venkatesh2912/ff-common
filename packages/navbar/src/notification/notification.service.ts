/**
 * @author Tapan Parekh
 * @class  NotificationService
 * This is a component wide service to get the resources for /notification via API call.
 */
import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { DatePipe } from '@angular/common';
// --------------------------------------- //
import { HttpRequestService } from '@fhlbny-ui-commons/core';
import { Notification } from './notification.model';


@Injectable()
export class NotificationService {

    public notificationUrl: string;
    constructor(
        private httpRequest: HttpRequestService,
        private datePipe: DatePipe
    ) {
        this.notificationUrl = '/notification/';
    }

    /**
     * Gets the list of notification. If reopen notification is set to false, it gets only the unclosed notifications else it gets closed + new.
     * @param reopenNotification A boolean indicating whether closed notifications are to be retrieved
     */
    getNotification(reopenNotification): Observable<any> {

        // Creates an observer
        return Observable.create(
            (observer) => {
                // Invokes the getRequest method and subscribe to its promise.
                this.httpRequest.getRequest(reopenNotification ? '/notification/?isClosed=true' : '/notification/', "").then(
                    // If the promise was resolved successfully.
                    (response) => {
                        observer.next(this.notificationMapper(response));
                        observer.complete();
                    },
                    // If the promise was rejected in case of any error.
                    (error) => {

                        observer.complete();
                    }
                );
            }
        );
    }

    /**
     * Closes a notification when a user clicks on x button.
     * @param id The id of the notification to be closed.
     * @param notification The notification object that is to be closed.
     */
    closeNotification(id, notification): Observable<any> {
        // Convert displayed date to a format acceptable by the server by creating a shallow copy and then assigning the date change
        let requestBody = Object.assign({}, notification);
        requestBody.createdDate = new Date(requestBody.createdDate);
        // Creates an observer
        return Observable.create(
            (observer) => {
                // Invokes the getRequest method and subscribe to its promise.
                this.httpRequest.putRequest(this.notificationUrl + id, requestBody, "").then(
                    // If the promise was resolved successfully.
                    (response) => {
                        observer.next(response);
                        observer.complete();
                    },
                    // If the promise was rejected in case of any error.
                    (error) => {
                        observer.error(error);
                        observer.complete();
                    }
                );
            }
        );
    }

    /**
     * Creates the desired request body and calls the API to mark the notification as read.
     * @param id The id of the notification to be marked as read
     * @param notification The notification object to be marked as read.
     */
    readNotification(id, notification): Observable<any> {
        // Convert displayed date to a format acceptable by the server by creating a shallow copy and then assigning the date change
        let requestBody = Object.assign({}, notification);
        requestBody.notificationRead = true;
        delete requestBody.createdDate;
        // Creates an observer
        return Observable.create(
            (observer) => {
                // Invokes the getRequest method and subscribe to its promise.
                this.httpRequest.putRequest(this.notificationUrl + id, requestBody,"").then(
                    // If the promise was resolved successfully.
                    (response) => {
                        observer.next(response);
                        observer.complete();
                    },
                    // If the promise was rejected in case of any error.
                    (error) => {
                        observer.error(error);
                        observer.complete();
                    }
                );
            }
        );
    }

    /**
     * Closes all the notifications.
     * @param ids The array of strings that needs to be deleted.
     */
    closeAllNotification(ids) {
        return Observable.create(
            (observer) => {
                // Invokes the getRequest method and subscribe to its promise.
                this.httpRequest.putRequest(this.notificationUrl, ids, "").then(
                    // If the promise was resolved successfully.
                    (response) => {
                        observer.next(response);
                        observer.complete();
                    },
                    // If the promise was rejected in case of any error.
                    (error) => {
                        observer.error(error);
                        observer.complete();
                    }
                );
            }
        );
    }

    /**
     * Downloads the file associated with the downloadable notification.
     * @param id The id of the notification for which the file needs to be downloaded
     */
    downloadFile(id): any {
        // Creates an observer
        return Observable.create(
            (observer) => {
                // Invokes the getRequest method and subscribe to its promise.
                this.httpRequest.getRequest(this.notificationUrl + 'download/' + id, "").then(
                    // If the promise was resolved successfully.
                    (response) => {
                        observer.next(response);
                        observer.complete();
                    },
                    // If the promise was rejected in case of any error.
                    (error) => {
                        observer.error(error);
                        observer.complete();
                    }
                );
            }
        );
    }

    /**
     * The notification mapper converts server's response to the one required by the ui.
     * It is also responsible to append the job id with the notification message. This function converts the time stamp to the required format. It also gets the desired
     * file name from the url sent by the server.
     * @param response The response received from the server
     */
    notificationMapper(response): Notification[] {
        let notification: Notification[] = [];
        response.forEach(
            (responseObj) => {
                notification.push(
                    new Notification(responseObj.notificationId,
                        responseObj.actionResource[0].actionTitle,
                        responseObj.actionResource[0].actionUrl,
                        responseObj.createdDate = this.datePipe.transform(responseObj.createdDate, 'MM/DD/YYYY HH:mm:ss'),
                        responseObj.closedDate ? this.datePipe.transform(responseObj.closedDate, 'MM/DD/YYYY HH:mm:ss') : '',
                        responseObj.notificationMessage += ' with job id: ' + responseObj.job.jobId,
                        responseObj.notificationRead,
                        responseObj.job.userId,
                        this.getFileName(responseObj.job.fileName)
                    ))
            });
        return notification;
    }

    /**
     * Extracts the file name from the url. This function handles the path sepeartor for linux as well as windows server.
     * @param fileName The url from which the filename is to be extracted
     */
    getFileName(fileName: string): string {
        if (fileName) {
            if (fileName.indexOf('/') > -1) {
                //linux
                fileName = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.length);
            }
            else {
                // windows
                fileName = fileName.substring(fileName.lastIndexOf("\\") + 1, fileName.length);
            }
            return fileName;
        } else {
            return '';
        }
    }
}
