import { MenuModel } from '@fhlbny-ui-commons/navbar';

export const appName: string = 'Capital Stock';

const stockProfolioMenuList = [
    new MenuModel('New', null, '/stock-portfolio'),
    new MenuModel('Open', null, '/stock-portfolio')
];

const reqReportsMenuList = [
    new MenuModel('Current Requirements', null, '/current-requirements'),
    new MenuModel('Exceptions Only', null, '/exceptions-only'),
];


const refrencetableMenuList = [
    new MenuModel('Dividend Calendar', null, '/dividend-calendar'),
    new MenuModel('Stock Classes', null, '/stock-classes'),
    new MenuModel('Stock Subclasses', null, '/stock-subclasses'),
    new MenuModel('General Ledger Accounts', null, '/general-ledger'),
];

const inqReportMenuList = [
    new MenuModel('Stockholder Summary', null, 'stock-holder-summary'),
    new MenuModel('Activity and Dividend', null, '/activity-and-dividend'),
    new MenuModel('Pending B1 Repurchases/Redemptions', null, '/pending-repurchases-redemptions'),
    new MenuModel('Redemption Notices', null, '/redemption-notices'),
    new MenuModel('Transaction Inquiry', null, '/transaction-inquiry'),
    new MenuModel('Transaction Journal', null, '/transaction-journal'),
    new MenuModel('Requirements Reports', reqReportsMenuList, null),
    new MenuModel('Stock Holdings', null, '/stock-holdings'),
    new MenuModel('Stock Holdings Summary', null, '/stock-holdings-summary'),
    new MenuModel('Dividend Summary', null, '/dividend-summary'),
    new MenuModel('Analysis of Dividend', null, '/analysis-of-dividend'),
    new MenuModel('Taxes Withheld on Dividend', null, '/taxes-withheld-on-dividend'),
    new MenuModel('Non-Member Holdings', null, '/non-member-holdings'),
    new MenuModel('Monthly Transaction Summary', null, '/monthly-transaction-summary'),
    new MenuModel('Monthly Redemptions/Repurchases', null, '/monthly-redemptions-repurchases'),
    new MenuModel('Election List', null, '/election-list'),
    new MenuModel('Daily Reports', null, '/daily-reports')
];

const utilitiesMenuList = [
    new MenuModel('Reference Tables', refrencetableMenuList, null),
    new MenuModel('Automatic Repurchases', null, '/automatic-repurchases'),
    new MenuModel('Year-End Data Extract', null, '/year-end-data-extract'),
    new MenuModel('Year-End MRA Upload', null, '/year-end-mra-upload'),
    new MenuModel('Membership Stock Purchases', null, '/membership-stock-purchase'),
    new MenuModel('Stockholder Re-Activation Link', null, '/stockholder-reactivation'),
    new MenuModel('Dividend Declaration', null, '/dividend-declaration'),
];

export const mainMenuList: MenuModel[] = [
    new MenuModel('Stock Portfolio', stockProfolioMenuList, null),
    new MenuModel('Inq/Reports', inqReportMenuList, null),
    new MenuModel('Utilities', utilitiesMenuList, null)
];