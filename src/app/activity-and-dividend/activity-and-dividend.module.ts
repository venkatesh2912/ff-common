import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, ActivatedRoute } from '@angular/router';
import {
   
    ActivityAndDividendComponent,
    ActivityAndDividendRoute
} from './';

const ENTITY_STATES = [
    ...ActivityAndDividendRoute,
];

@NgModule({
    imports: [
        RouterModule.forChild(ENTITY_STATES),
    ],
    declarations: [
        ActivityAndDividendComponent
    ],
    entryComponents: [
        ActivityAndDividendComponent
        
    ],
    providers: [
        
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ActivityAndDividendModule {}
