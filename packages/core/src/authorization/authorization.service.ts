/**
 * @author Faizal Vasaya
 * A canactivate service that performs authorization check for the currently logged in user. This service also checks whether a link is to be
 * displayed to the user or not.
 */
import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
// --------------------------------------- //
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable()
export class AuthorizationService implements CanActivate {
  // Store the list of modules that the user has access to.
  public modules: Array<string>;
  public functions: Array<string>;

  constructor(private authService: AuthenticationService, private router: Router) {
    this.modules = [];
    this.functions = [];
    // Subscribe to userdetails to get its current status.
    this.authService.userDetails.subscribe((authStatus) => {
      this.modules = authStatus.modules;
      authStatus.moduleFunctionMapping.forEach(element => {
        this.functions.push(element.functionName);
      });
    });
    // Extract required parameters when user logs in from prompt.
    this.modules = this.authService.user.modules;
    this.authService.user.moduleFunctionMapping.forEach(element => {
      this.functions.push(element.functionName);
    });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.modules.indexOf(state.url) == -1) {
      this.router.navigate(['/accessdenied']);
    }
    // Gets the users list of authorized modules and compares with the currently accessing url. Refere MDN docs for string.indexof() method
    return (this.modules.indexOf(state.url) > -1);
  }

  /**
   * Checks whether an element should be visible to the logged in user or not.
   * @param functions The functions to be checked for its visibility
   */
  isElementVisible(functions: string[]): boolean {
    let isVisible = false;
    functions.forEach((item) => {
      isVisible = (isVisible || this.functions.indexOf(item) > -1);
    });
    return isVisible;
  }
}
