/**
 * @author Faizal Vasaya
 * @description A module to register fhlbny application wide components.
 */
import { CommonModule, TitleCasePipe } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
// --------------------------------------- //
import { LoginComponent } from './login/login.component';
import { ErrorComponent } from './error/error.component';
import { RouterModule } from '@angular/router';
// --------------------------------------- //
export { LoginComponent } from './login/login.component';
export { ErrorComponent } from './error/error.component';
// Commneted as barrel files are not supported by production/aot build of webpack initiated by yarn build
export * from './toaster/index';
export * from './form/index';
export * from './spinner/index';
export * from './filtering/index';
export * from './panel/index';
export * from './export/index';
export * from './table/index';
export * from './table/pagination/index';
export * from './table/sort/index';
export * from './modal/index';
export * from './error/index';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    declarations: [
        LoginComponent,
        ErrorComponent
    ],
    exports: [
        LoginComponent,
        ErrorComponent
    ],
    entryComponents: [
    ]
})
export class ComponentsModule {
    /**
   * This static method is used to load all the services registered with core module.
   */
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ComponentsModule,
            providers: [
            ]
        }
    }
}
