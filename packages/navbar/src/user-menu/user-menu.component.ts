/**
 * @author Faizal Vasaya
 * @description A component that deals with user menu displayed in the topbar. It is a child component of <fhlby-topbar>.
 */
import { Component, OnInit, Output, EventEmitter, TemplateRef } from '@angular/core';
import { NgbModal, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// --------------------------------------- //

import { EnvironmentConfigService } from '@fhlbny-ui-commons/core';
import { AuthenticationService } from '@fhlbny-ui-commons/core';
import { ToasterService, ToasterType, ToasterTitle } from '@fhlbny-ui-commons/components';
import { Router } from '@angular/router';
import { Signature } from '@fhlbny-ui-commons/core';

@Component({
  selector: 'fhlbny-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {

  // Stores the userName of the currently logged in user.
  public userName: string;
  // The profile image of the logged in user.
  public userImage: string;
  // Current environment in which the application is deployed in .
  public environment: string;
  // Store the status of whether the signature was available on the server or not
  public isSignatureAvailable: boolean;
  // Stores a boolean indicating the show/hide status of delete confirm box
  public isDeleteConfirmBoxVisible: boolean;
  // Toggle enable/disable of upload button.
  public disableUpload: boolean;
  // Event emitter to emit user logout action
  @Output()
  public userLoggedOut: EventEmitter<boolean> = new EventEmitter<boolean>();
  // A temporary variable to store user signature since authservice is private and it cannot be accessible during AOT mode in html.
  public image: string;

  public modalRef: NgbModalRef;
  constructor(
    private authService: AuthenticationService,
    private environmentConfigService: EnvironmentConfigService,
    private toasterService: ToasterService,
    private modalService: NgbModal,
    private router: Router
  ) {
    this.disableUpload = true;
    this.environment = this.environmentConfigService.environment.name;
    this.isSignatureAvailable = false;
  }

  ngOnInit() {
    // To get the username initialized during single signon.
    this.userName = this.authService.user.name;
    // To get the userImage initialized during single signon.
    this.userImage = this.authService.user.image;
    // TO get the userdetails initialized using custom login
    this.authService.userDetails.subscribe(
      (userDetails) => {
        this.userName = userDetails.name;
        this.userImage = userDetails.image;
      }
    );
  }

  /**
   * Opens the modal as per the content passed in it parameter
   * @param content The template to be rendered in the modal
   */
  open(content: any) {
    content.open();
    //this.modalRef = this.modalService.open(content, { centered: true, backdrop: "static" });
  }

  /**
   * Closes the currently opened modal
   */
  close(content: any) {
    content.close();
  }

  /**
   * Dismisses the currently opened modal
   */
  dismiss() {
    this.modalRef.dismiss();
  }

  /**
   * Toggle's the signature modal and invokes the getUserSignature service to get the currently logged in user's signature.
   */
  getSignature() {
    // checks if signature id exists for the user
    this.authService.getUserSignature().then(
      (signature) => {
        this.image = signature.image;
        if (signature.image !== null) {
          this.authService.user.signature = {
            id: signature.id,
            image: signature.image,
            userName: signature.userName,
            crteDate: signature.crteDate,
            modifiedDate: signature.modifiedDate
          };
          this.isSignatureAvailable = true;
        } else {
          this.isSignatureAvailable = false;
          this.authService.user.signature = {};
        }
      },
      (error) => {
        this.toasterService.toast({
          type: ToasterType.Error,
          title: ToasterTitle.Error,
          message: 'Error in retriving signature.'
        });
      }
    );
  }


  /**
   * This method is triggered when the signature file is changed. It validates that the signature file is of one of the accepted types
   * and its size is less than or equal to 200kb.
   * @param event The event sent by the filechange event.
   */
  signatureChanged(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let fileExtension: string = file.name.substring(file.name.lastIndexOf('.'), file.name.length);
      if (['.png', '.jpeg', '.jpg', '.gif'].indexOf(fileExtension.toLowerCase()) > -1) {
        if (file.size / 1024 <= 200) {
          this.disableUpload = false;
          let myReader: FileReader = new FileReader();
          myReader.onloadend = (e) => {
            this.authService.user.signature.image = myReader.result;
            this.image = this.authService.user.signature.image;
          }
          myReader.readAsDataURL(file);
        } else {
          this.toasterService.toast({
            type: ToasterType.Error,
            title: ToasterTitle.Error,
            message: 'File size should be less than 200kb.'
          });
        }
      }
      else {
        this.toasterService.toast({
          type: ToasterType.Error,
          title: ToasterTitle.Error,
          message: 'Only png, jpeg, jpg and gif file formats are allowed.'
        });

      }
    }
  }

  /**
   * This method is invoked when the upload button is clicked. It calls the authService's uploadSignature to send the signature to the server
   * and shows toasts as per the status of the upload.
   */
  uploadSignature() {
    this.authService.uploadSignature().then(
      () => {
        this.disableUpload = true;
        this.isSignatureAvailable = false;
        this.toasterService.toast({
          type: ToasterType.Success,
          title: ToasterTitle.Success,
          message: 'Signature saved successfully!'
        });

      },
      () => {
        this.toasterService.toast({
          type: ToasterType.Error,
          title: ToasterTitle.Error,
          message: 'Error in uploading signature.'
        });
      }
    );
  }

  /**
   * This method invokes the application wide delete service to delete the user's signature.
   */
  deleteSignature() {
    this.authService.deleteSignature().then(
      (response) => {
        this.authService.user.signature = {};
        this.image = this.authService.user.signature.image;
        this.isSignatureAvailable = false;
        this.toasterService.toast({
          type: ToasterType.Success,
          title: ToasterTitle.Success,
          message: 'Signature deleted successfully!'
        });
      },
      (error) => {
        this.toasterService.toast({
          type: ToasterType.Error,
          title: ToasterTitle.Error,
          message: 'Error in deleting signature.'
        });
      }
    )
  }

  /**
   * Invokes the logout service and navigates the user back the login route.
   */
  logout() {
    this.authService.logoutUser().then(
      () => {
        this.userLoggedOut.emit(true);
      }
    );
    this.router.navigate(['/login']);
  }
}
