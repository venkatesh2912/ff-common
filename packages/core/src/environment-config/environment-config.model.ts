export class EnvironmentConfig {
    baseUrl: string;
    name: string;
    rolePrefix: string;
    version: string;
}