export class ErrorDetail {

    timestamp?: String;
    status?: string;
    error?: string;
    message?: string;
    path?: string;

}