/**
 * @author Faizal Vasaya & Tapan Parekh
 * This service is an http interceptor that intercepts each http requests initiated by the application.
 */
import { Injectable, Inject, Injector } from '@angular/core'; //Used injector since APP_INITIALIZER indirectly uses authService and RequestInterceptor also
// used the same authService which results in cyclic dependency
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/mergeMap';
// --------------------------------------- //
import { AuthenticationService } from '../authentication/authentication.service';
import { HttpRequestService } from '../http-request/http-request.service';
import { SingleSignOnService } from '../single-sign-on/single-sign-on.service';
import { ErrorAlertService } from '../error-alert/error-alert.service';

@Injectable()
export class RequestInterceptorService implements HttpInterceptor {
    public authService: AuthenticationService;
    public cachedRequest: Array<HttpRequest<any>> = [];
    public requestCounter;
    constructor(injector: Injector, private singleSignOnService: SingleSignOnService, private errorAlertService: ErrorAlertService) {
        this.requestCounter = 0;
        // A hack to avoid maximum call stack exceeded error.
        setTimeout(() => {
            this.authService = injector.get(AuthenticationService);
        });
    }
    /**
     * Success Response mechanism:
     * The intercept method intercepts each XHR request made by the angular application and appends the required headers to the request. The application
     * will generate a preflight 401 error if this header is appended to the initial user authentication request, hence it performs a special check for
     * the authentication XHR call made to UserServices/authUser. It also appends special 'responseType' key to the request when the server needs to send
     * a blob for the requested URL. A check for i18's json files needs to be made as it does not requires any token as such.
     *
     * 401 error mechanism:
     * If the server response with a 401 unauthorized access then a a refresh token call will be issued. This will be when the current user is a single signed on user.
     * If a user has logged in using login prompt then the user has to click on yes from the single sign on prompt inorder to refresh the token for the system's user.
     *
     * @param req The current request to be intercepted.
     * @param next The next handler in the interceptor array to which the request will be passed to.
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Checks for the authentication request. If true appends an option of withCredentials to true and passes on the request.
        if (req.url.indexOf('auth') > -1) {
            req = req.clone({
                withCredentials: true
            });
        } else if (req.url.indexOf('login') > -1) {
            // This section of code is executed when the user logs in using login prompt.
            req = req.clone({
                headers: req.headers.set('Content-Type', 'application/x-www-form-urlencoded')
            })
        } else if (req.url.indexOf('i18n') > -1) {
        }
        // Checks whether the url contains exportType in its query parameters and appends the required 'responseType' key to it.
        else if ((req.body !== null && req.body.exportType !== undefined && req.body.exportType !== '' && req.url.indexOf('job') === -1) || req.url.indexOf('download') > -1) {
            req = req.clone({
                responseType: 'blob',
                setHeaders: {
                    'PHX-Authorization': `Bearer ${this.authService.user.accessToken}`,
                    'Accept': `application/vnd.app.v1+json`
                }
            });
        } // For all other request except authentication and file export
        else {

            req = this.addAuthToken(req);
        }
        // Pass on the request to the next request handler. If the response was successfull request counter is set to 0. If an error occured
        // of type 401, then a refresh token is obtained and the new token is attached to the old request.
        return next.handle(req).do(
            (event) => {
                if (event instanceof HttpResponse) {
                    this.requestCounter = 0;
                }
            }
        ).catch(
            (error: any) => {
                if (error.url.indexOf('login') == -1) {
                    this.errorAlertService.showError(error.error);
                }
                // Check whether the reponse is of type 401 and was not issued by '/login' call.
                if (error instanceof HttpErrorResponse && (<HttpErrorResponse>error).status === 401 && this.requestCounter < 5 && error.url.indexOf('login') == -1) {
                    //Checks whether the user is a Sngle signed on user or not. If the user is a single signed on user, then its request counter will be incremented and the token will be refreshed.
                    // After successfull token refresh, the previouly failed call will continue.
                    if (this.authService.user.isSSOUser) {
                        this.requestCounter++;
                        // Wait until refresh token is successfully obtained.
                        return this.authService.refreshToken().flatMap(
                            () => {
                                // Continue with previous failed call.
                                return next.handle(this.addAuthToken(req));
                            }
                        );
                    } else {
                        // Show pop up on the UI.
                        this.singleSignOnService.emitIsPopUpShown(true);
                        // Wait untill user submits the pop up's response.
                        return this.singleSignOnService.popUpResponse.flatMap(
                            () => {
                                this.requestCounter++;
                                // Wait until refresh token is successfully obtained.
                                return this.authService.refreshToken().flatMap(
                                    () => {
                                        // The token was refreshed successfully. This is used in app.component.ts to refresh the page.
                                        this.authService.emitTokenRefreshed();
                                        // Continue with previous failed call.
                                        return next.handle(this.addAuthToken(req));
                                    }
                                );
                            }
                        );
                    }
                }
            }
        );
    }

    /**
     * This method appends additional headers to the req.
     * @param req The request to which the new token is to be attached.
     */
    addAuthToken(req: HttpRequest<any>): HttpRequest<any> {
        req = req.clone({
            setHeaders: {
                'PHX-Authorization': `Bearer ${this.authService.user.accessToken}`,
                'Accept': `application/vnd.app.v1+json`
            }
        });
        return req;
    }
}
