import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PageSizeSelectorComponent, PaginationComponent } from './pagination/index';
import { TableComponent } from './table.component';
import { PropertyValuePipe } from './property-value.pipe';
import { SortByDirective, SortDirective } from './sort/index';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    declarations: [
        PageSizeSelectorComponent,
        PaginationComponent,
        TableComponent,
        PropertyValuePipe,
        SortByDirective,
        SortDirective
    ],
    exports: [
        PageSizeSelectorComponent,
        PaginationComponent,
        TableComponent
    ]
})
export class TableModule {}
