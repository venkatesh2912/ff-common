import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorAlertComponent } from './error-alert.component';
import { ErrorAlertService } from './error-alert.service';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
    imports: [
        CommonModule,
        NgbAlertModule.forRoot()
    ],
    declarations: [
        ErrorAlertComponent
    ],
    exports: [
        ErrorAlertComponent
    ]
})
export class ErrorAlertModule {
    /**
  * This static method is used to load all the services registered with core module.
  */
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ErrorAlertModule,
            providers: [
                ErrorAlertService
            ]
        }
    }
}
