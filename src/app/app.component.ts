import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { appName, mainMenuList } from './topbar-demo/topbar.config';
import { MenuModel } from '@fhlbny-ui-commons/navbar';
import { AuthenticationService, EnvironmentConfigService } from '@fhlbny-ui-commons/core';
import { ToasterService, ToasterType, ToasterTitle } from '@fhlbny-ui-commons/components';
import { SpinnerService } from '@fhlbny-ui-commons/components';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, AfterViewInit {
    public appName: string;
    public mainMenuList: MenuModel[];
    public showTopbar: boolean;
    constructor(
        private authService: AuthenticationService,
        private environmentConfigService: EnvironmentConfigService,
        private router: Router,
        private toaster: ToasterService,
        private spinner: SpinnerService
    ) {
        this.appName = appName;
        this.mainMenuList = mainMenuList;
    }

    ngOnInit() {
        // To get values to decide whether to show menu or not
        this.showTopbar = this.authService.user.isAuthenticated && this.authService.user.isAuthorized;
        // TO get the userdetails initialized using custom login
        this.authService.userDetails.subscribe(
            (userDetails) => {
                this.showTopbar = userDetails.isAuthenticated && userDetails.isAuthorized;
            }
        );
        //this.showTopbar = true;
    }

    ngAfterViewInit() {
        //Demo for toast
        this.toaster.toast({
            message: 'Hey',
            type: ToasterType.Success,
            title: ToasterTitle.Success
        });
        // Demo for spinner
        this.spinner.spin(true);
        setTimeout(
            () => {
                this.spinner.spin(false);
            }, 3000
        )
    }

    /**
    * The method invoked when the userloggedout event occurs from user-menu child component.
    */
    onUserLoggedOut($event) {
        this.router.navigate(['/login']);
    }
}
