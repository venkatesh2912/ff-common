/**
 * @author Faizal Vasaya
 * This service handles an observable subject which accepts a boolean that is responsible to toggle the single sign on UI. This service is
 * used is app.component to toggle the single sign on confirm box.
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
// --------------------------------------- //


@Injectable()
export class SingleSignOnService {
    // An observable to toggle pop up.
    public isPopUpShown: Subject<boolean>;
    public popUpResponse: Subject<boolean>;
    constructor() {
        this.isPopUpShown = new Subject<boolean>();
        this.popUpResponse = new Subject<boolean>();
    }

    emitIsPopUpShown(status) {
        this.isPopUpShown.next(status);
    }

    emitPopUpResponse(response: boolean) {
        this.popUpResponse.next(response);
    }
}
