export interface FilterField {
    name: string;
    label: string;
    placeholder?: string;
    inputType?: 'text' | 'number' | 'select' | 'date' | 'dateBetween';
    // Only applies to the "select" inputType
    possibleValues?: String[];
    value?: any;
    minLength?: number,
    maxLength?: number,
    [x: string]: any;
}
