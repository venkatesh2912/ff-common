/**
 * @author Faizal Vasaya
 * @description A module to configure and register navigation components.
 */
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders, Optional, SkipSelf } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatePipe } from '@angular/common';
import { NgbModalModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
// --------------------------------------- //

import { MenuModel } from './menu/menu.model';
import { TopbarComponent } from './topbar/topbar.component';
import { MenuComponent } from './menu/menu.component';
import { NotificationComponent } from './notification/notification.component';
import { NotificationService } from './notification/notification.service';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { PipesModule } from '@fhlbny-ui-commons/pipes';
import { ComponentsModule } from '@fhlbny-ui-commons/components';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ModalModule } from '@fhlbny-ui-commons/components';
// --------------------------------------- //

export { MenuModel } from './menu/menu.model';
export { TopbarComponent } from './topbar/topbar.component';
export { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    PipesModule,
    ModalModule,
    ComponentsModule.forRoot(),
    NgbModalModule.forRoot(),
    NgbDropdownModule.forRoot(),
  ],
  declarations: [
    TopbarComponent,
    MenuComponent,
    NotificationComponent,
    UserMenuComponent,
    BreadcrumbComponent
  ],
  providers: [
    NotificationService,
    DatePipe
  ],
  exports: [
    TopbarComponent,
    BreadcrumbComponent
  ]
})
export class NavbarModule {}
