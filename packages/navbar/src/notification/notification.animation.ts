import { state, trigger, transition, style, animate, AnimationTriggerMetadata } from '@angular/animations';

export const notificationSlide: AnimationTriggerMetadata = trigger('slide', [

    // --> Using Right
    state('slideOut', style({
        right: '-100%'
    })),
    state('slideIn', style({
        right: '0'
    })),

    transition('slideOut <=> slideIn', animate('0.5s ease-in-out'))

]);