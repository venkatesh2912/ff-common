/**
 * @author Faizal Vasaya
 * @desc Authorizes whether an html element is visible to the user based on the array of functions passed as an input.
 * @example <div *hasPermission="['/acs-to-gl-reconciliation-maintenance/add']"></div>
 */
import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
//-------------------------------//

import { AuthorizationService } from './authorization.service';


@Directive({
    selector: '[hasPermission]'
})
export class HasPermissionDirective {

    /**
     * Initilaizes the function names and invokes the update view method
     */
    @Input('hasPermission')
    set permissions(value: string[]) {
        this.updateView();
    }

    constructor(
        private authorizationService: AuthorizationService,
        private templateRef: TemplateRef<any>,
        private viewContainerRef: ViewContainerRef
    ) {

    }

    /**
     * Clears the view container and renders it back if the user has the permission.
     */
    private updateView(): void {
        this.viewContainerRef.clear();
        if (this.authorizationService.isElementVisible(this.permissions)) {
            this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
    }
}