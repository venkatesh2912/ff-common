/**
 * @author Faizal Vasaya
 * Application wide boilerplate code for performing REST API calls.
 */
import { Injectable, Inject, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http'; //Note: Here httpClient which is available from angular 4.3 is used instead of http.
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
// --------------------------------------- //
import { EnvironmentConfigService } from '../environment-config/environment-config.service';
import { environmentMap } from '../environment-config/environment-config.constant';

@Injectable()
export class HttpRequestService {
    // The url of the hosted application. Its value is derieved from the type of environment the application is currently running in.
    public baseUrl: string;
    constructor(private httpClient: HttpClient, private environmentConfigService: EnvironmentConfigService) {
        // Get the base url from provided environment config
        this.environmentConfigService.environmentDetails.subscribe(
            (environment) => {
                this.baseUrl = environment.baseUrl;
            }
        );
    }

    /**
     * This method is used to perform application wide get requests. It returns a promise which when resolved passes on the response object to the request
     * initiator.
     * @param requestUrl The URL to which the get request needs to issued to.
     */
    getRequest<T>(requestUrl, appName: string): Promise<T> {
        let getRequestPromise = new Promise<T>(
            (resolve, reject) => {
                // Perform get request on the sepecified URL. This request will be further intercepted by application wide HTTP interceptor which adds the
                // authentication relted parameters.
                // Generate an http request of delete type on the specified URL and subscribe to it to recieve its response.
                this.httpClient.get<T>(this.baseUrl + ':' + environmentMap.get(appName) + requestUrl).subscribe(
                    // Callback triggered when the response was successfully received from the server. It resolves the promise and passes on the data.
                    (responseObject) => {
                        resolve(responseObject)
                    },
                    // Callback triggered when an error object was sent by the server. It rejects the promise and passes on the error.
                    (error: HttpResponse<any>) => {
                        reject(error);
                    }
                );
            }
        );
        return getRequestPromise;
    }

    /**
     * This method generates a get request for the url passsed in its parameters. This is a special get method used only in case of obtaining
     * refresh token.
     * @param requestUrl The url to which the request is to be made to.
     */
    getRequestObservable(requestUrl, appName: string): Observable<any> {
        return Observable.create(
            (observer) => {
                this.httpClient.get(this.baseUrl + ':' + environmentMap.get(appName) + requestUrl).subscribe(
                    (response) => {
                        observer.next(response);
                        observer.complete();
                    },
                    (error) => {
                        observer.error(error);
                        observer.complete();
                    }
                );
            });
    }

    /**
     * The method generates a delete request for the request url passed received in its parameters. It returns a promise which resolves when the
     * request was successful and the recieved object is passed on to the method waiting for this promise.
     * @param requestUrl  The url to which the request is to be made to.
     */
    deleteRequest(requestUrl, appName: string): Promise<any> {
        // Creates a new promise and store it in a function wide variable.
        let deleteRequestPromise = new Promise<any>(
            (resolve, reject) => {
                // Generate an http request of delete type on the specified URL and subscribe to it to recieve its response.
                this.httpClient.delete(this.baseUrl + ':' + environmentMap.get(appName) + requestUrl).subscribe(
                    // Callback triggered when the response was successfully received from the server. It resolves the promise and passes on the data.
                    (responseObject) => {
                        resolve(responseObject)
                    },
                    // Callback triggered when an error object was sent by the server. It rejects the promise and passes on the error.
                    (error) => {
                        reject(error);
                    }
                );
            }
        );
        return deleteRequestPromise;
    }

    /**
     * This method is an application wide post request service. It accepts the requestUrl and the body which is passed on to the REST API url.
     * Each request made using this method is intercepted by application wide http request interceptor in request-interceptor.service.ts file.
     * It returns a promise which needs to be thened (subscribed) to receive the response object on success.
     * @param requestUrl The url to which the request has to me made to.
     * @param body       The request body to be sent for the post request to the URL.
     */
    postRequest<T>(requestUrl, body, appName: string): Promise<T> {
        // Creates a new promise and store it in a function wide variable
        let postRequestPromise = new Promise<T>(
            (resolve, reject) => {
                // Generate an http request of post type on the specified URL and subscribe to it to recieve its response.
                this.httpClient.post<T>(this.baseUrl + ':' + environmentMap.get(appName) + requestUrl, body).subscribe(
                    // Callback triggered when the response was successfully received from the server. It resolves the promise and passes on the data.
                    (responseObject) => {
                        resolve(responseObject)
                    },
                    // Callback triggered when an error object was sent by the server. It rejects the promise and passes on the error.
                    (error) => {
                        reject(error);
                    }
                );
            }
        );
        return postRequestPromise;
    }

    /**
     * This method is an application wide put request service. It accepts the requestUrl and the body which is passed on to the REST API url.
     * Each request made using this method is intercepted by application wide http request interceptor in request-interceptor.service.ts file.
     * It returns a promise which needs to be thened (subscribed) to receive the response object on success.
     * @param requestUrl The url to which the request has to me made to.
     * @param body       The request body to be sent for the post request to the URL.
     */
    putRequest<T>(requestUrl, body, appName: string): Promise<T> {
        // Creates a new promise and store it in a function wide variable
        let putRequestPromise = new Promise<T>(
            (resolve, reject) => {
                // Generate an http request of put type on the specified URL and subscribe to it to recieve its response.
                this.httpClient.put<T>(this.baseUrl + ':' + environmentMap.get(appName) + requestUrl, body).subscribe(
                    // Callback triggered when the response was successfully received from the server. It resolves the promise and passes on the data.
                    (responseObject) => {
                        resolve(responseObject)
                    },
                    // Callback triggered when an error object was sent by the server. It rejects the promise and passes on the error.
                    (error) => {
                        reject(error);
                    }
                );
            }
        );
        return putRequestPromise;
    }
}
