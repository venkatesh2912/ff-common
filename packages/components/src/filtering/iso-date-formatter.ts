import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

/*
 * Converts to/from a ng-bootstrap datepicker NgbDateStruct object/UTC ISO string that they can be passed
 * to the backend API.
 *
 * NOTE: This could probably be included as a part of a utils package
 */
export class IsoDateFormatter {

    static toIsoString(date: NgbDateStruct): string {
        const utcDateObject = new Date(Date.UTC(date.year, date.month - 1, date.day));
        const localDateObject = new Date(utcDateObject.getTime() + utcDateObject.getTimezoneOffset() * 60000);

        return localDateObject.toISOString();
    }

    static fromIsoString(date: Date | string): NgbDateStruct {
        if (!date) {
            return null;
        }

        let dateObject;

        if (date instanceof Date) {
            dateObject = date;
        } else {
            dateObject = new Date(date);
        }

        return { day: dateObject.getUTCDate(), month: dateObject.getUTCMonth() + 1, year: dateObject.getUTCFullYear() };
    }
}
