/**
 * @author Faizal Vasaya
 * @class  NumberInputComponent
 * The NumberInputComponent handles the an input type with number
 */
import { Component, OnInit, Input, HostListener, ViewChild, ElementRef, Renderer2, AfterViewInit, OnDestroy } from '@angular/core';
import { FormControl, ControlContainer, FormGroupDirective, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/merge';
import { Subscription } from 'rxjs/Subscription';

// --------------------------------------- //
import { ErrorMessage } from '../form.model';

@Component({
    selector: 'fhlbny-number-input',
    templateUrl: './number-input.component.html',
    styleUrls: [
        './number-input.component.scss'
    ]
})

export class NumberInputComponent implements OnInit, AfterViewInit, OnDestroy {

    // The form control to which the input belongs to.
    @Input()
    public control: FormControl;

    // The label of the input
    @Input()
    public label: string;

    // An array of error messages corresponding to the input
    @Input()
    public errorMessages: ErrorMessage[];

    // AN element ref of the input element to apply styling to the input box
    @ViewChild('numberInput', { read: ElementRef }) numberInput: ElementRef;

    // An observable to observe whether the parent form is submitted or not
    formSubmit: Observable<any>;

    // A boolean to indicate form submission status
    isFormSubmitted: boolean;

    // An observable to identify a change in value of the control
    controlValue: Observable<any>;

    // A subcription that listens to formload, formsubmit and value control changes event
    controlSubscription: Subscription;

    // A boolan indicating whether to show the error or not
    isErrorMessageShown: boolean;

    // The current error message to be shown for this control
    currentErrorMessage: string;

    constructor(
        private cc: ControlContainer,
        private renderer: Renderer2
    ) {
    }

    ngOnInit() {
        // Observes for the for to get submitted
        this.formSubmit = (<FormGroupDirective>this.cc).ngSubmit.map(
            () => {
                this.isFormSubmitted = true;
            }
        );
        // An observable to observe change in value of control + form load + form submit
        this.controlValue = Observable.merge(this.control.valueChanges, Observable.of(''), this.formSubmit);
        // A subscription to observe control value observer
        this.controlSubscription = this.controlValue.subscribe(
            () => {
                this.setClasses();
            }
        )
    }

    ngAfterViewInit() {
        // Append validators to the existing set of validators for the control
        this.control.setValidators(Validators.compose([this.control.validator, Validators.pattern(/^\d+$/)]));
    }

    // Prevent keypress event when a key other than 0-9 is pressed
    @HostListener('keypress', ['$event'])
    onKeyPress($event) {
        if (!this.isNumber($event.key)) {
            event.preventDefault();
        }
    }

    // Validate a number
    isNumber(input: string): boolean {
        const reg = /^\d+$/;
        if (!reg.test(input)) {
            return false;
        }
        return true;
    }

    // Applies classes to the controls based on the current state of the control and the form
    setClasses() {
        if (this.control.invalid && (this.control.dirty || this.isFormSubmitted)) {
            this.renderer.addClass(this.numberInput.nativeElement, 'is-invalid');
            this.isErrorMessageShown = true;
            this.setCurrentErrorMessage();
        } else {
            if (this.control.valid && this.control.dirty) {
                this.renderer.addClass(this.numberInput.nativeElement, 'is-valid');
                this.isErrorMessageShown = false;
            }
            this.renderer.removeClass(this.numberInput.nativeElement, 'is-invalid');
        }
    }

    // Sets error message for the control
    setCurrentErrorMessage() {
        for (let i = 0; i < this.errorMessages.length; i++) {
            if (this.control.hasError(this.errorMessages[i].error)) {
                this.currentErrorMessage = this.errorMessages[i].message;
                break;
            }
        }
    }

    // Clear the subscription for performance improvements
    ngOnDestroy() {
        this.controlSubscription.unsubscribe();
    }
}