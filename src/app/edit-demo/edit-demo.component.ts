/**
 * @author Faizal Vasaya
 * @class  EditDemoComponent
 * The EditDemoComponent to demo the use of reusable edit component
 */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// --------------------------------------- //
import { FormDescription, InputType, ButtonType, FormType } from '@fhlbny-ui-commons/components';


@Component({
    selector: 'edit-demo',
    templateUrl: './edit-demo.component.html'
})

export class EditDemoComponent implements OnInit {
    demoForm: FormGroup;
    formDescription: FormDescription[];
    formType: FormType

    constructor(
        private fb: FormBuilder,
        private router: Router
    ) {
        this.formType = FormType.update;
    }

    ngOnInit() {
        this.createForm();
    }

    createForm() {
        this.demoForm = this.fb.group({
            numberInput: [{ value: '', disabled: true }, [Validators.required]],
            stringInput: ['', [Validators.required]],
            dateInput: ['', [Validators.required]],
            selectInput: [null, [Validators.required]]
        });
        this.formDescription = [
            {
                label: "Number Input",
                control: this.demoForm.get('numberInput'),
                type: InputType.number,
                errorMessages: [
                    { error: 'required', message: 'Number Input is required' },
                    { error: 'pattern', message: 'Number Input is not a valid number' }
                ]
            },
            {
                label: "String Input",
                control: this.demoForm.get('stringInput'),
                type: InputType.string,
                errorMessages: [
                    { error: 'required', message: 'String Input is required' }
                ]
            },
            {
                label: "Date Input",
                control: this.demoForm.get('dateInput'),
                type: InputType.date,
                errorMessages: [
                    { error: 'required', message: 'Date Input is required' }
                ]
            },
            {
                label: "Select Input",
                control: this.demoForm.get('selectInput'),
                type: InputType.select,
                values: [{
                    id: 1,
                    value: 'Sample 1'
                },
                {
                    id: 2,
                    value: 'Sample 2'
                }],
                errorMessages: [
                    { error: 'required', message: 'Select Input is required' }
                ]
            }
        ];
        this.demoForm.patchValue(
            {
                numberInput: '1256',
                stringInput: 'Hey',
                dateInput: '12/04/2018',
                selectInput: {
                    id: 1,
                    value: 'Sample 1'
                }
            }
        )
    }

    buttonClicked(event: ButtonType) {
        if (event === ButtonType.add) {
            console.log(this.demoForm.getRawValue());
        } else if (event === ButtonType.update) {
            console.log("update value",this.demoForm.getRawValue());
        } else if (event === ButtonType.cancel) {
            console.log('Cancel was clicked');
        } else if (event === ButtonType.delete) {
            console.log('Delete was clicked');
            console.log(this.demoForm.getRawValue());
        }
        this.router.navigate(['/list'], { queryParamsHandling: "preserve" });
    }

}