/**
 * @author Faizal Vasaya
 * @description Do not register anything in this module. It just serves as a medium to export public api's
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// --------------------------------------- //

import { QueryParamsStateService } from './query-params-state/query-params-state';
// --------------------------------------- //

export * from './query-params-state/index';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [

    ],
    exports: [

    ]
})
export class PipesModule {
}
