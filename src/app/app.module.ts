/**
 * @author Faizal Vasaya
 * @description A module to test commons components
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { HashLocationStrategy, Location, LocationStrategy } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbRootModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
// ------------------------------//
import { AppComponent } from './app.component';
import { NavbarModule } from '@fhlbny-ui-commons/navbar';
// tslint:disable-next-line:max-line-length
import { RequestInterceptorService, singleSignOn, AuthenticationService, EnvironmentConfigService, CoreModule, ErrorHandlerInterceptor, ErrorAlertModule } from '@fhlbny-ui-commons/core';
import { LoginComponent, ComponentsModule, ToasterModule, SpinnerModule, FormModule, PanelModule, ExportModule, FilteringModule, TableModule, ModalModule, ErrorComponent } from '@fhlbny-ui-commons/components';
import { EditDemoComponent } from './edit-demo/edit-demo.component';
import { ListPanelDemoComponent } from './list-panel-demo/list-panel-demo.component';
import { ActivityAndDividendModule } from './activity-and-dividend/activity-and-dividend.module';
import { ListDemoService } from './list-panel-demo/list-panel-demo.service';

const LOGIN_ROUTES: Routes = [
    { path: '', component: ListPanelDemoComponent },
    { path: 'list', component: ListPanelDemoComponent },
    { path: 'edit', component: EditDemoComponent },
    { path: 'login', component: LoginComponent },
    { path: 'accessdenied', component: ErrorComponent },
    { path: 'error', component: ErrorComponent },
    { path: '**', redirectTo: '/error' }
];

@NgModule({
    declarations: [
        AppComponent,
        EditDemoComponent,
        ListPanelDemoComponent
    ],
    imports: [
        BrowserModule,
        NgbModule.forRoot(),
        NavbarModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        CoreModule.forRoot(),
        ComponentsModule.forRoot(),
        ToasterModule.forRoot(),
        ErrorAlertModule,
        SpinnerModule.forRoot(),
        FormModule.forRoot(),
        FilteringModule,
        TableModule,
        PanelModule,
        ModalModule,
        ExportModule,
        RouterModule.forRoot(LOGIN_ROUTES),
        ActivityAndDividendModule
    ],
    providers: [
        Location,
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: RequestInterceptorService,
            multi: true
        },
        {
            provide: APP_INITIALIZER,
            useFactory: singleSignOn,
            multi: true,
            deps: [AuthenticationService, EnvironmentConfigService]
        }, ListDemoService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
