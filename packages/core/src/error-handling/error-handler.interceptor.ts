import { HttpInterceptor, HttpRequest, HttpErrorResponse, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { Router } from '@angular/router';
import { Injector, Injectable } from '@angular/core';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
    router: Router;
    constructor(private injector: Injector) {
        setTimeout(() => {
            this.router = injector.get(Router);
        });
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).do((event: HttpEvent<any>) => { }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if ((err.status === 403 || err.status === 404) && err.url.indexOf('notification') === -1) {
                    this.router.navigate(['/error',err.error]);
                }
            }
        });
    }
}
