import { Component, EventEmitter, Input, Output } from '@angular/core';

import { PageData } from './pagination/page-data.model';
import { TableField } from './table-field.model';
import { SortConfig } from './sort/sort-config.model';

@Component({
    selector: 'fhlbny-table',
    templateUrl: './table.component.html'
})
export class TableComponent {

    @Input()
    fields: TableField[];

    @Input()
    items: any[];

    @Input()
    pageData: PageData;

    @Input()
    sortConfig: SortConfig;

    @Output()
    onPageChange: EventEmitter<number> = new EventEmitter<number>();

    public pageChange(pageNum: number) {
        this.onPageChange.emit(pageNum);
    }
}
