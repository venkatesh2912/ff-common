export class MenuModel {
    public name: string;
    public subMenuList: MenuModel[];
    public routerLink: string;

    constructor(name: string, subMenuList: MenuModel[], routerLink: string) {
        this.name = name;
        this.subMenuList = subMenuList;
        this.routerLink = routerLink;
    }

}