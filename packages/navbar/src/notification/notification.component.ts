/**
 * @author Faizal Vasaya
 * @description A component that deals with notification displayed in the topbar. It is a child component of <fhlby-topbar>.
 */
import { Component, OnInit, Output, EventEmitter, Input, Renderer2 } from '@angular/core';
import * as FileSaver from 'file-saver';
// --------------------------------------- //

import { notificationSlide } from './notification.animation';
import { AuthenticationService } from '@fhlbny-ui-commons/core';
import { NotificationService } from './notification.service';
import { ToasterService } from '@fhlbny-ui-commons/components';
import { ToasterType,ToasterTitle } from '@fhlbny-ui-commons/components';

@Component({
  selector: 'fhlbny-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  animations: [
    notificationSlide
  ]
})
export class NotificationComponent implements OnInit {
  // Stores an instance of interval
  public interval: any;
  // Stores the list of notification
  public notificationList: Array<any> = [];
  // displaying notifciations based on filters.
  public filteredList: Array<any> = [];
  // Stores a boolean to indicate the show/hide status of new/closed notifications.
  reopenNotification: boolean;
  // Stores a boolean indicating whether a user is currently authenticated or not.
  public isAuthenticated: boolean;
  // Stores the count of unread notification
  public unreadNotificationCount: number;
  // Previous unread notification count
  public previousUnreadNotificationCount: number;
  // sets the initial state of the notification pane to be hidden (Added by: Shezad)
  public state: string = 'slideOut';
  // filtering of notification (Added by: Shezad)
  public filters: string[];
  public selectedFilter: string = 'all';
  // An event emmiter that emits whenever a new notification is available
  @Output() isNotificationAvailable: EventEmitter<any>;

  constructor(
    private notificationService: NotificationService,
    private authService: AuthenticationService,
    private renderer: Renderer2,
    private toaster: ToasterService
  ) {
    this.reopenNotification = false;
    this.unreadNotificationCount = 0;
    this.previousUnreadNotificationCount = 0;
    this.isNotificationAvailable = new EventEmitter<any>();
    this.filters = ['all', 'read', 'unread'];
  }

  ngOnInit() {
    // Checks wheher the user is authenticated or not. If yes it calls a function to fetch notification
    this.isAuthenticated = this.authService.user.isAuthenticated;
    if (this.isAuthenticated) {
      setTimeout(() => {
        this.fetchNotification();
      });
    }
    // TO get the userdetails initialized using custom login
    this.authService.userDetails.subscribe(
      (userDetails) => {
        this.isAuthenticated = userDetails.isAuthenticated;
        if (userDetails.isAuthenticated) {
          this.fetchNotification();
        }
      }
    );
  }

  /**
   * Fetches the list of notification based on the reopen flag. It also creates a timer that calls the function to fetch the notification every 60 seconds.
   */
  fetchNotification() {
    this.notificationService.getNotification(this.reopenNotification).subscribe(
      (NotificationList) => {
        this.manageNotification(NotificationList);
      },
      (error) => {

      },
      () => {
        if (this.isAuthenticated) {
          this.interval = setTimeout(() => {

            if (this.isAuthenticated) {
              this.fetchNotification();
              clearTimeout(this.interval);
            }
          }, 60000);
        }
      }
    );
  }

  /** gets the notification based on the type checked. (Created by: Shezad)
   @param type filter type */
  getNotifications(type) {
    if (type == 'read') {
      this.filteredList = this.notificationList.filter((notification) => {
        return notification.notificationRead === true;
      })
    } else if (type == 'unread') {
      this.filteredList = this.notificationList.filter((notification) => {
        return notification.notificationRead === false;
      })
    } else {
      this.filteredList = this.notificationList;
    }
  }

  /**
   * Gets the list of notifications and calls manage notification on success.
   */
  updateNotification() {
    this.notificationService.getNotification(this.reopenNotification).subscribe(
      (notificationList) => {
        this.manageNotification(notificationList);
      },
      (error) => {

      }
    );
  }

  /**
   * Manages the count of unread notifications to show/hide the notificatiion indicator.
   * @param notificationList The list of notification retrieved from the server
   */
  manageNotification(notificationList) {
    this.unreadNotificationCount = 0;
    // modified by Shezad
    this.filteredList = notificationList;
    this.notificationList = notificationList;
    if (this.notificationList.length > 0) {
      this.notificationList.forEach((element, index) => {
        if (!element.notificationRead) {
          this.unreadNotificationCount++;
        }
      });
      // Show toaster when new unread notifications are fetched from the server
      if (this.unreadNotificationCount > this.previousUnreadNotificationCount) {
        this.toaster.toast({
          type: ToasterType.Info,
          title : ToasterTitle.Info,
          message: `You have ${this.unreadNotificationCount} notifications.`
        });
      }
      this.previousUnreadNotificationCount = this.unreadNotificationCount;
    }
    // Emmits an event when there are any unread notification.
    if (this.unreadNotificationCount > 0) {
      this.updateNotificationAvailability(true);
    }
    else {
      this.updateNotificationAvailability(false);
    }
  }

  /**
   * Emits the current availability of notification. It emits true when the notification is available.
   * @param status Current availability status. True if new notifications are available.
   */
  updateNotificationAvailability(status) {
    this.isNotificationAvailable.emit(status);
  }

  /**
   * Downloads the file for the respective notification
   */
  downloadFile(notification) {
    //this.loader.emitIsLoaderShown(true);
    this.notificationService.downloadFile(notification.id).subscribe(
      (file) => {
        FileSaver.saveAs(new Blob([file], { type: file.type }), notification.fileName);
        //this.loader.emitIsLoaderShown(false);
        this.readNotification(notification.id, notification);
        // this.toasterService.pop('success', 'Success', 'File downloaded successfully!');
      },
      (error) => {
        // this.toasterService.pop('error', 'Error', 'Error in downloading file.');
        // this.loader.emitIsLoaderShown(false);
      }
    );
  }

  /**
   * This function is invoke when the user clicks on close notification 'x' button.
   * @param id The for the notification to be closed
   * @param notification The notification object to be marked as closed
   */
  closeNotification(id, notification) {
    notification.closedDate = new Date();
    this.notificationService.closeNotification(id, notification).subscribe(
      () => {
        this.updateNotification();
      });
  }

  /**
   * Marks the notification as read when the user downloads the file.
   * @param id The id of the notification to be marked as read.
   * @param notification The notification object that needs to be marked as read.
   */
  readNotification(id, notification) {
    this.notificationService.readNotification(id, notification).subscribe(
      () => {
        this.updateNotification();
      });
  }

  /**
   * Sends the list of notification ids that are to be marked as read.
   */
  clearAllNotification() {
    var notificationIdList = [];
    this.notificationList.forEach(element => {
      notificationIdList.push(element.id);
    });
    this.notificationService.closeAllNotification(notificationIdList).subscribe(
      () => {
        this.notificationList = [];
      }
    );
  }

  /**
   * Reopens closed notifications.
   */
  reOpenNotification() {
    this.reopenNotification = !this.reopenNotification;
    this.updateNotification();
  }

  // hides the notification pane (Created by: Shezad)
  hideNotificationPane() {
    this.state = 'slideOut';
  }

  /**
   * filters the notifciations based on the arguments (Created by: Shezad)
   * @param event
   * @param filter
   */
  filterNotifications(event, filter) {
    if (event.target !== 'checked') {
      this.selectedFilter = filter;
    }
    this.getNotifications(event.target.value);
  }

  // mark all notifications as read (Created by: Shezad)
  markAllRead() {
    this.getNotifications('unread');
    // this.notificationList.markAllRead(this.filteredList).subscribe(
    //   () => {

    //   }
    // )
  }
}
