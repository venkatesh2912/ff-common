import { Component, ContentChild, Input, TemplateRef, OnInit } from '@angular/core';
import { ErrorDetail } from './error.model';
import { ActivatedRoute, Params, Route, NavigationEnd, Router, RoutesRecognized } from '@angular/router';

@Component({
    selector: 'fhlbny-error',
    templateUrl: './error.component.html',
})
export class ErrorComponent implements OnInit {

    previousUrl = "";
    currentURL = "";
    constructor(private actRoute: ActivatedRoute, private route: Router) {
        this.currentURL = route.url;
    }

    ngOnInit() {
        this.route.events.filter(e => e instanceof RoutesRecognized)
        .pairwise()
        .subscribe((event: any[]) => {
            this.previousUrl = event[0].urlAfterRedirects;
        });
    }

}
